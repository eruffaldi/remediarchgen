
doc/       contains the tool documentation
examples/  contains the examples in YAML
generated/ provides the generated PDF and DOC
remedi/    provides the YAML of remedi architecture
