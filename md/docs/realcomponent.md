#D_DiagUI_Coco3D
Description: 
Autor: SSSA

**Inputs**

* DDM_Status:std_msgs::UInt32 - A status sent by Decision maker
* Command:CocoCmdID - Change a work mode of an application
* Kinect_Stream:kinect_stream - 

**Outputs**

* Status
#D_DiagUI_Coco4Body
Description: 
Autor: SSSA

**Inputs**

* DDM_Status:std_msgs::UInt32 - A status sent by Decision maker
* Command:CocoCmdID - Change a work mode of an application
* Tomography_Measures:TomographyMeasures - 

**Outputs**

* Status
#D_DiagUI_DecisionMaker
Description: Decision Maker is a kind of Finite State Machine, which decides whether the DiagUI is ready for examination and reacts in the case of the Error
Autor: WRUT

**Inputs**

* AssiAppState:AppState - A message sent by the G-DiagUi assistant application to inform that the application is alive and ready for connection
* PatiAppState:AppState - A message sent by the G-DiagUi patient application to inform that the application is alive and ready for connection
* Assi2DoctAVState:AVState - It is received when state of an audio or video stream sent by the G-DiagUI assistant application is changed
* Pati2DoctAVState:AVState - It is received when state of an audio or video stream sent by the G-DiagUI patient application is changed
* Remedi_IND:GDiagUI2Matlab - It contains information about the state of individual indicators
* EMG:std_msgs::UInt8 - Emergency input
* Arm_Feedback:ArmFeedback - It sent by a ReMeDi robot. This is a complex signal with structure which consists of: Arm_Pose (cartesian position and orientation of end-effector), Joint_Pos (joints angle position), Wrench (end-effector measured forces and torques), Tool_Type (tool type actually connected to Arm: NONE, STHETOSCOPE, PALPATION_EFFECTOR, USG)
* DGD_Status:std_msgs::UInt8 - G-DiagUI Interface status
* Coco4Body:std_msgs::UInt8 - Coco4Body status
* Coco3D:std_msgs::UInt8 - Coco3D status
* PECS_Status:std_msgs::UInt8 - Palpation Interface Control System
* Delta_Status:std_msgs::UInt8 - Haptic Interface Control System
* Phenix_Status:std_msgs::UInt8 - Haptic Interface Control System

**Outputs**

* Status
#D_DiagUI_GdiagUI_DoctorApp
Description: It is used by a doctor to perform a patient examination and communication with an assistant and a patient
Autor: WRUT

**Inputs**

* AssiAppState:AppState - A message sent by the G-DiagUi assistant application to inform that the application is alive and ready for connection
* PatiAppState:AppState - A message sent by the G-DiagUi patient application to inform that the application is alive and ready for connection
* Assi2DoctAVState:AVState - It is received when state of an audio or video stream sent by the G-DiagUI assistant application is changed
* Pati2DoctAVState:AVState - It is received when state of an audio or video stream sent by the G-DiagUI patient application is changed
* DDM_Status:std_msgs::UInt32 - A status sent by Decision maker
* PowerManager:PowerManager - Is sent by ReMeDi robot to show a doctor a power level of robot batteries. The signal is a structure which consists of 2 uint8: Battery_PowerLevel and PowerManager_Status (values: Power_ON, Power_OFF, Charging_in_Progress) i.e. PowerManager.Battery_PowerLevel
* Arm_Feedback:ArmFeedback - It sent by a ReMeDi robot. This is a complex signal with structure which consists of: Arm_Pose (cartesian position and orientation of end-effector), Joint_Pos (joints angle position), Wrench (end-effector measured forces and torques), Tool_Type (tool type actually connected to Arm: NONE, STHETOSCOPE, PALPATION_EFFECTOR, USG)
* Remedi_IND:GDiagUI2Matlab - It is received when a robot state is changed
* PainLevel:std_msgs::Bool - Information about patient pain detection
* PatientAudio:audio_stream - An audio stream sent from a patient site sent by the patient G-DiagUI application
* PatientVideo:video_stream - A video stream from a patient site sent by the patient G-DiagUI application
* AssistantAudio:audio_stream - An audio stream sent from a patient site sent by the assistant G-DiagUI application
* AssistantVideo:video_stream - A video stream from a patient site sent by the assistant G-DiagUI application
* USG_Image:video_stream - A video stream from a patien site sent by the USG device
* CardioVideo:video_stream - A video stream from a patien site sent by the Cardiograph device. This screen of that device
* AuscultationAudio:audio_stream - An audio stream sent from a patient site sent by the patient G-DiagUI application. It receives audio from a stethoscope

**Outputs**

* DoctAppState
* Doct2AssiAVState
* Doct2PatiAVState
* Remedi_IND
* PatientVideo
* PatientAudio
* AssistantVideo
* AssistantAudio
* DGD_Status
#D_DiagUI_HeadJoy
Description: The remedi_head_joy
Autor: WRUT

**Inputs**

* joy:sensor_msgs::Joy - Input from joystick

**Outputs**

* head_vel
#D_DiagUI_Palpation_Effector
Description: Component controlling palpation effector
Autor: UWE

**Inputs**

* DDM_Status:std_msgs::UInt32 - A status sent by Decision maker

**Outputs**

* Status
#D_DiagUI_Palpation_Phenix
Description: ROS node for Phenix haptic interface (1kHz)
Autor: SSSA

**Inputs**

* endeffector_set_force:geometry_msgs::WrenchStamped - Set the force (wrench) applied at the end-effector
* endeffector_set_pose:geometry_msgs::PoseStamped - Set the position
* endeffector_set_velocity:geometry_msgs::TwistStamped - Set the linear velocity of the end-effector
* endeffector_set_controll_mode:std_msgs::UInt32 - Change the controll mode of the interface: 1 – torque, 2 – force, 3 -- joint angles (disabled in this node), 4 -- angular velocit, 5 – position, 6 – velocity.

**Outputs**

* endeffector_position
* endeffector_velocity
* endeffector_force
* endeffector_status
#D_DiagUI_Palpation_Telemanipulation
Description: Component performing USG telemanipulation
Autor: UWE

**Inputs**

* DDM_Status:std_msgs::UInt32 - A status sent by Decision maker

**Outputs**

* Status
#D_DiagUI_USGControlPanel
Description: ROS node for control of USG Device
Autor: ACC

**Outputs**

* Cursor_XY
* Pressed_Keys
* Mouse_Buttons
#D_DiagUI_USG_Delta
Description: ROS node for Delta haptic interface (1kHz)
Autor: SSSA

**Inputs**

* endeffector_set_force:geometry_msgs::WrenchStamped - Set the force (wrench) applied at the end-effector
* endeffector_set_pose:geometry_msgs::PoseStamped - Set the position
* endeffector_set_velocity:geometry_msgs::TwistStamped - Set the linear velocity of the end-effector
* endeffector_set_controll_mode:std_msgs::UInt32 - Change the controll mode of the interface: 1 – torque, 2 – force, 3 -- joint angles (disabled in this node), 4 -- angular velocit, 5 – position, 6 – velocity.

**Outputs**

* endeffector_position
* endeffector_velocity
* endeffector_force
* endeffector_status
#D_DiagUI_USG_Telemanipulation
Description: ROS node for performing USG telemanipulation
Autor: WRUT

**Inputs**

* in_force_sub_:geometry_msgs::WrenchStamped - Desired force to be generated on the delta
* in_pose_topic_:geometry_msgs::PoseStamped - Desired pose of end effector
* in_velocity_topic_:geometry_msgs::TwistStamped - Desired velocity of the end effector
* in_controll_mode_topic_:std_msgs::UInt32 - Desired controll mode
* in_enc_zero_sub:std_msgs::Bool - Demand to introduce the offset in the top encoders angles
* in_status_sub_:ros_gdiagui::MsgToDiagUI - Status of robot
* in_pose_pub_:geometry_msgs::PoseStamped - realized delta position
* in_velocity_pub_:geometry_msgs::TwistStamped - realized delta velocity
* in_force_pub_:geometry_msgs::WrenchStamped - realized delta force
* in_status_pub_:percro_6dofhaptic::Status - Publishes the status, in particular the state of foot pedal

**Outputs**

* out_force_sub_
* out_pose_topic_
* out_velocity_topic_
* out_controll_mode_topic_
* out_enc_zero_sub
* out_status_sub_
* out_pose_pub_
* out_velocity_pub_
* out_force_pub_
* out_status_pub_