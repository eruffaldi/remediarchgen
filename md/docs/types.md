#Message AVState
* VideoStreamState:uint8 - A state of a video stream: 0 – no transmission, 1 – is transmitted
* AudioStreamState:uint8 - A state of an audio stream: 0 – no transmission, 1 – is transmitted
#Message AppState
* AppState:uint8 - A state of the application which sends the message that it is: 0 – not read, 1 – active
#Message ArmFeedback
* Arm_Pose:float[7] - 
* Joint_Pos:float[7] - 
* Wrench:float[6] - Measured Forces and Torques in WC
* Tool_Type:uint8 - Values: NONE, STHETOSCOPE, PALPATION_EFFECTOR, USG
#Message Bool
#Message CocoCmdID
* CmdID:uint8 - An identifier of a command
#Message Cursor_XY
* Cursor_X:int16 - Position of mouse cursor in x direction
* Cursor_Y:int16 - Position of mouse cursor in y direction
#Message GDiagUI2Matlab
* MasterSlave_Sync_OK:bool - Synchronization of a robot
* HW_Fault_IND:bool - Hardware fault
* Platf_HW_Fault_IND:bool - 
* Platf_ManualMove_IND:bool - 
* Platf_RemoteMove_IND:bool - 
* Platf_Stop_IND:bool - 
* Head_HW_Fault_IND:bool - 
* Head_ManualCtrl_IND:bool - 
* Head_RemoteCtrl_IND:bool - 
* Head_HoldPose_IND:bool - 
* Arm_HW_Fault_IND:bool - 
* Arm_HandLeadCtrl_IND:bool - 
* Arm_RemoteCtrl_IND:bool - Robot's Arm Ready for Remote Examination
* Arm_HoldPose_IND:bool - 
* Arm_Parking_IND:bool - 
* ToolChange_IND:bool - 
* Remote_Examination_IND:bool - Robot's Arm remotely controlled by Doctor
* Configuration_IND:bool - 
* MeasuredForce:uint8 - 
* JointLimit_IND:bool - 
* PalpationEffector_HW_Fault_IND:bool - 
* DummyValue:uint8[] - 
#Message Joy
#Message Mouse_Button
* Mouse_Buttons:uint8[3] - Mouse buttons statuses ON/OFF
#Message Mouse_Buttons
#Message MsgToDiagUI
#Message PoseStamped
#Message PowerManager
* Battery_PowerLevel:uint8 - Batery level
* PowerManager_Status:uint8 - Values:Power_ON, Power_OFF, Charging_in_Progress
#Message Pressed_Keys
* Pressed_Keys:uint16[5] - Codes of pressed keys
#Message Status
* header:Header - 
* status:uint32 - 
* button_press:uint32 - 
#Message TomographyMeasures
#Message TwistStamped
#Message UInt32
#Message UInt8
#Message WrenchStamped
#Message audio_stream
#Message geometry_msgs::PoseStamped
#Message geometry_msgs::TwistStamped
#Message geometry_msgs::WrenchStamped
#Message kinect_stream
#Message percro_6dofhaptic::Status
#Message ros_gdiagui::MsgToDiagUI
#Message sensor_msgs::Joy
#Message std_msgs::Bool
#Message std_msgs::UInt32
#Message std_msgs::UInt8
#Message video_stream